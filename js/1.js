var palabras = ["araña","perro","aguacate","manzana", "avion", "camaleon", "barco", "coche","cocodrilo","moto"]; // debe contener las palabras en MINÚSCULAS
var pista = ["Animal", "Animal", "Fruta", "Fruta", "Medio de transporte", "Animal", "Medio de transporte", "Medio de transporte", "Animal", "Medio de transporte"];
var longitud;
var posicion;
var lIntroducidas = []; // array que contiene las letras introducidas;
var acertaste = 0; //almacena el número de letras que acierta para saber cuándo ha acertado la palabra completa
var posicionAcertada = [];
window.addEventListener("load", () => {
    cargar();
    /* *********************************** ALEATORIO *********************************** */

    /* Devuelve un número entre 0 y la longitud del array de palabras que es la posición de la palabra aleatoria*/
    function aleatorio(arg) { // arg es el array que contiene las palabras (palabras)
        var x = 0; // almacena la posición de la palabra aleatoria
        var l = 0; // longitud del array
        l = arg.length;
        x = parseInt(Math.random() * l);
        return x;

    }


    /* *********************************** CARGAR *********************************** */

    function cargar() {
        lIntroducidas = [];
        posicion = aleatorio(palabras); //posición de la palabra elegida al azar
        var p = pista[posicion];
        document.querySelector(".pista").innerHTML += "<p>Pista:  " + p + "</p>";

        for (var i = 1; i < 6; i++) {
            document.querySelector("#id" + i).style.opacity = 1 //Tapar el muñeco
        }
        /* Longitud de la palabra */

        longitud = palabras[posicion].length; //Longitud de la palabra


        dibujar(longitud);
    }

    /* *********************************** DIBUJAR CAJAS *********************************** */
    /* Dibujamos tantas cajas como letras tenga la palabra*/
    function dibujar(arg) {
        var v = []; //array de divs

        v = document.querySelector(".fila1")
        for (var i = 0; i < longitud; i++) {
            v.innerHTML += "<div class='letras'></div>";

        }
    }

    /* *********************************** LEER CARACTER INTRODUCIDO *********************************** */

    /* Creamos la función que lee los caracteres */
    var c = "";

    document.querySelector("#comprobar").addEventListener("click", (e) => {
        c = (document.querySelector("#letra").value).toLowerCase(); // leo el valor. Lo paso a minúsculas
        document.querySelector("#letra").value = ""; //limpio el input
        comprobar(c); //compruebo la letra  
    })


    /* *********************************** ¿ESTÁ INTRODUCIDO? *********************************** */

    /* Compruebo si la letra está introducida */
    function estaIntroducida(arg) { // le paso el caracter y comprueba si está introducida
        for (var i = 0; i < lIntroducidas.length; i++) {
            if (arg == lIntroducidas[i]) {
                return 1;
            }
        }

    }

    var posiciones = [];


    /* *********************************** ¿ESTÁ CONTENIDO EN LA PALABRA? *********************************** */

    function comprobar(arg) { //Le pasamos la letra que introduce el usuario
        var si = estaIntroducida(c); // si devuelve 1 está introducida
        var str = ""; //contiene la palabra con la que jugamos
        var contador = 0; //Almacena cuántas veces no coincide la letra en el string

        str = palabras[posicion];


        // Compruebo si está el carácter en la palabra y guardo las posiciones

        /* Letra ya introducida */

        if (si == 1) {
            alert("La letra ya está introducida.");
        } else {

            /* Letra nueva */

            for (var i = 0; i < longitud; i++) { //Almacena en el array posiciones, las posiciones donde coincida el carácter
                if (arg == str[i]) { //coincide la letra
                    posiciones.push(i);
                    posicionAcertada.push(i);
                    acertaste++;

                } else { //no coincide la letra
                    contador++;
                }

            }
            if (contador == longitud) {
                escribirFallos(arg); //si la letra no está contenida en la palabra la escribe en fallos

            } else {
                escribirAciertos(arg, posiciones);
            }

            lIntroducidas.push(arg);

            if (acertaste == longitud) {
                var m=document.querySelector(".mensaje");
                m.style.opacity=1;
                m.style.backgroundColor="#00FF90FF";
                m.innerHTML+="¡Acertaste la palabra!";

                setTimeout(function() {
                    alert("Pulse aceptar si desea volver a jugar");
                    location.reload();
                }, 1000);


            }
        }
    }

    /* Reseteo las variables */


    var acumulador = 0; // Almacena el número de coincidencias
    var contador = 0;


    /* *********************************** ESCRIBIR ACIERTOS *********************************** */

    /* Función para escribir los caracteres que acierta el usuario */
    function escribirAciertos(arg1, arg2) { // arg1 es la letra que coincide, arg2 es el array con las posiciones de la letra de arg1
        var a = 0; //almacena la posición del array de posiciones

        for (var i = 0; i < arg2.length; i++) {
            var a = arg2[i];
            var b = document.querySelectorAll(".letras");

            b[a].innerHTML += arg1.toUpperCase();
            b[a].style.backgroundColor = "#2ECC71";
        }

        contador = arg2.length;
        posiciones = []; //limpiar el array

        acumulador += contador;
        if (acumulador == longitud) {
            console.log("fin");
        }
    }

    /* *********************************** ESCRIBIR FALLOS *********************************** */

    /* Función para escribir los caracteres que falla el usuario */
    var f = 0; //posiciones de las cajas de los fallos

    function escribirFallos(arg) {
        v = document.querySelector(".fila2");
        v.innerHTML += "<div class='letrasF'></div>";
        document.querySelectorAll(".letrasF")[f].innerHTML += arg.toUpperCase();

        f++;


        if (f < 6) {
            descubrir(f);
        }

        if (f == 5) {
            document.querySelector("#id5").style.opacity = 0;
            console.log("posicion acertada", posicionAcertada);

            /* ************** !!! PERDISTE  !!!**************   */

            var cajas = document.querySelectorAll(".letras");
            var aux = 0;
            var ordenado = posicionAcertada.sort();
            var v = [];
            console.log("ordenado", ordenado);

            console.log("posiciones v", v);


            var aux = 0;

            /* Pongo en rojo las que me faltaban por acertar y cumplimento la palabra */
            for (var i = 0; i < palabras[posicion].length; i++) {
                cajas[i].innerHTML="";
                cajas[i].innerHTML+=palabras[posicion][i].toUpperCase();
                cajas[i].style.backgroundColor = "red";
                cajas[i].style.color = "white";
            }

            for(var i=0;i<ordenado.length;i++){
                cajas[ordenado[i]].style.backgroundColor = "green";
                cajas[ordenado[i]].style.color = "black";
            }
            var m=document.querySelector(".mensaje");
                m.style.opacity=1;
                m.style.fontSize="3em";
                m.style.backgroundColor="#FEA9A9FC";
                m.style.color="red";
                m.innerHTML+="¡Perdiste! La palabra correcta era: " + palabras[posicion].toUpperCase();

            
                setTimeout(function() {
                    alert("Pulse aceptar si desea volver a jugar");
                    location.reload();

                }, 1000);

            
        }

    }


    function descubrir(arg) {
        document.querySelector("#id" + arg).style.opacity = 0;

    }


    /* ***************************************** Pulsando letras **************************** */
    window.addEventListener("keydown", (e) => {
        var x = event.keyCode || event.which;
        console.log(x);
        if (x == 192) {
            c = "ñ";
            comprobar("ñ");

        } else if ((x >= 97 && x <= 122) || (x >= 65 && x <= 90) || (x >= 48 && x <= 57)) {
            var y = String.fromCharCode(x).toLowerCase();
            c = y;
            comprobar(y);

        }
        console.log("lintroducidas", lIntroducidas);

    })

})